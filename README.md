## Overview

This project contains two example scripts that are used for showing how to implement monkeypatching in the testing of these scripts using pytest.

## Tools/Modules

This project requires Python 3.6 or greater (f-strings are awesome!).

Key python modules used:

- Requests - easily work with HTTP verbs (GET, POST, DELETE, PATCH)
- pytest - my preferred testing framework in python
 
## How to Run

In the top-level directory, activate the virtual environment:

    $ souce venv/bin/activate

If you need to install the required python modules:

    $ pip install -r requirements.txt

Run each script individually:

    $ python test_example1.py
    $ python test_example2.py

## Testing

In the top-level directory, activate the virtual environment:

    $ pytest -v

## References

I highly recommend Brian Okken's book (Python Testing with py.test).  This book taught me so much about pytest!
